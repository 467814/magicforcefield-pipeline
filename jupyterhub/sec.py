async def bootstrap_pre_spawn(spawner):
    spawner.container_security_context = {"capabilities": {"drop": ["ALL"]}}
